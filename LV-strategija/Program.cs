﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV_strategija
{
    class Program
    {
        static void Main(string[] args)
        {
            NumberSequence sequence = new NumberSequence(new double[] { 8,6,1,-6,18,47,24,3,19,-40,57,0 });
            sequence.SetSortStrategy(new SequentialSort());
            sequence.Sort();
            Console.WriteLine(sequence);

            NumberSequence sequence2 = new NumberSequence(new double[] { 8, 6, 1, -6, 18, 47, 24, 3, 19, -40, 57, 0 });
            sequence2.SetSortStrategy(new CombSort());
            sequence2.Sort();
            Console.WriteLine(sequence2);

            NumberSequence sequence3 = new NumberSequence(new double[] { 8, 6, 1, -6, 18, 47, 24, 3, 19, -40, 57, 0 });
            sequence3.SetSortStrategy(new BubbleSort());
            sequence3.Sort();
            Console.WriteLine(sequence3);

            sequence.SetSearchStrategy(new LinearSearch());

            Console.WriteLine(sequence.Search(8) + "\n");

        }
    }
}
