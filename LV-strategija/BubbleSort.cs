﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV_strategija
{
    class BubbleSort : SortStrategy
    {
        public override void Sort(double[] array)
        {
            int arraySize = array.Length;
            for (int i = 0; i < arraySize; i++)
            {
                for (int j = 0; j < arraySize - 1; j++)
                {
                    if (array[j] > array[j + 1])
                    {
                  
                        Swap(ref array[j+1], ref array[j]);
                    }
                }
            }
        }
    }
}
