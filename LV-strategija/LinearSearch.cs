﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV_strategija
{
    class LinearSearch : SearchStrategy
    {
        public override int Search(double[] array, double target)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == target)
                {
                    Console.WriteLine("Found on index ");
                    return i;
                }
            }
            return -1;
        }
    }
}
